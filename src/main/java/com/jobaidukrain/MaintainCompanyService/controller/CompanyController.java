package com.jobaidukrain.MaintainCompanyService.controller;

import com.jobaidukrain.MaintainCompanyService.Dto.companyAdressDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class CompanyController {

    @GetMapping("/maintain-your-company")
    public String getMaintainCompany(Model model) {
        model.addAttribute("companyAdressDto", new companyAdressDto());
        return "maintain_company";
    }

    @PostMapping("/company/update")
    public String updatecompany(@ModelAttribute("companyAdressDto")@Valid companyAdressDto companyAdressDto, Model model){

        return "maintain_company";
    }
}