package com.jobaidukrain.MaintainCompanyService.Dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class companyAdressDto {

    @NotEmpty(message = "Name may not be empty")
    private String name;
    @NotEmpty
    @Email
    private String email;
    private String url;
    private String logo;
    private addressDto addressDto;

    public companyAdressDto(){

    }
}
