package com.jobaidukrain.MaintainCompanyService.Dto;

import lombok.Data;

@Data
public class addressDto {

    private float lng;
    private float lat;
    private String street;
    private int houseNumber;
    private int zip;
    private String city;
    private  String country;

}
