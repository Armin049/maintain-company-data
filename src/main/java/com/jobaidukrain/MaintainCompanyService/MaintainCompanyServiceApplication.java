package com.jobaidukrain.MaintainCompanyService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaintainCompanyServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaintainCompanyServiceApplication.class, args);
	}

}
